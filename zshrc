export ZSH=/home/Trib/.oh-my-zsh
ZSH_THEME="bullet-train"
DISABLE_AUTO_UPDATE="true"
COMPLETION_WAITING_DOTS="true"

# Would you like to use another custom folder than $ZSH/custom?
ZSH_CUSTOM=/home/Trib/.oh-my-zsh/themes/bullet-train.zsh-theme

# Add wisely, as too many plugins slow down shell startup.
plugins=(git rails ruby command-not-found gem npm rvm)

# User configuration
export PATH="/home/Trib/bin:/usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/usr/games"
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh

export EDITOR='vim'
export USE_EDITOR=$EDITOR
export VISUAL=$EDITOR

# ooh, what is this? Aliases?
source .oh-my-zsh/lib/alias.zsh

# Disable auto-correct
# unsetopt correct_all

# Load up chruby, auto-load ruby version, and set default
# source /usr/local/opt/chruby/share/chruby/chruby.sh
# chruby ruby-2.3

# Always work in a tmux session if tmux is installed
# if which tmux 2>&1 >/dev/null; then
#   if [ $TERM != "screen-256color" ] && [  $TERM != "screen" ]; then
#       tmux attach -t hack || tmux new -s hack; exit
#   fi
# fi

export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting
